import numpy as np
import matplotlib
from pandas import read_csv


def load_data(filename):
    """
    Load data from a csv file

    Parameters
    ----------
    filename : string
        Filename to be loaded.

    Returns
    -------
    X : ndarray
        the data matrix.

    y : ndarray
        the labels of each sample.
    """
    data = read_csv(filename)
    z = np.array(data)
    y = z[:, 0]
    X = z[:, 1:]
    return X, y


def split_data(X, y, tr_fraction=0.5):
    """Split the data X,y into two random subsets."""

    num_samples = y.size
    n_tr = int(num_samples * tr_fraction)
    n_ts = num_samples - n_tr

    idx = np.array(range(0, X.shape[0]))
    np.random.shuffle(idx)

    tr_idx = idx[0:n_tr]
    ts_idx = idx[n_tr: n_tr + num_samples]

    Xtr = X[tr_idx, :]
    ytr = y[tr_idx]

    Xts = X[ts_idx, :]
    yts = y[ts_idx]

    return Xtr, ytr, Xts, yts


def count_digits(y):
    """Count the number of elements in each class."""
    num_classes = np.unique(y).size  # number of unique elements in y
    p = np.zeros(shape=(num_classes,), dtype=int)
    x = np.unique(y)

    for k in xrange(num_classes):
        p[k] = np.sum(y == x[k])
    return p


def fit(Xtr, ytr):
    """Compute the average centroid for each class.
    :param Xtr:
    :param ytr:
    :return:
    """
    num_classes = np.unique(ytr).size
    centroids = np.zeros(shape=(num_classes, Xtr.shape[1]))
    for k in xrange(num_classes):
        xk = Xtr[ytr == k, :]
        centroids[k, :] = np.mean(xk, axis=0)
    return centroids
